Introduction
============

There are many excellent books on Python, but many do not really cover
Python from a Scientist's or number crunchers view. This book was written
to fix that problem.