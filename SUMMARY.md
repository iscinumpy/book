# Summary

* [Introduction](README.md)
* [Installing software](chapter1.md)
* [Getting started with Python](getting-started-with-python.md)
* [Chapter 3](chapter-3.md)
* [Chapter 4](chapter-4.md)
* [Chapter GUI](chapter-gui.md)
* [Appendix 1](appendix-1.md)

