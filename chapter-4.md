Introduction to Numpy
=====================

Arrays
------

From here on I’ll constantly refer to C and C code (and maybe even C++).
You don’t have to know how to write C, just know that it’s a lower level
language that allows you to write code that gets converted directly to
computer language. So it is clean and very fast, from the computer’s
point of view. It can be convoluted and lengthy to write, though. If you
do know how to write C, I’ll provide several ways for you to apply that
knowledge later in special sections. But I’m not about to teach C here.

So, why use Numpy instead of Python lists and built-ins? There are three
reasons that pop into my mind:

1.  Size. Python’s list stores a complete copy of each Python object.
    So, at the very least, you have info like “I am an int” in every
    single cell. Millions of copies makes this very bulky. Also, you get
    very little control over the type of number stored. In Numpy, you
    can chose standard computer datatypes, like `float32`, `float64`,
    `int8`, `int32`, `uint32`, etc. If you know your needed float
    precision or max/min int size, you can optimize the memory
    usage/storage.

2.  Speed. Working with intricate Python objects, one at a time, is slow
    for a computer. But Numpy is mostly written in C, so Numpy array
    operations are very, very fast. They are vectorized (so the computer
    can compute small chunks at once if it supports it), and they loop
    in the C part (fast) instead of the Python part (slow). So you can
    gain a couple of orders of magnitude if you can rewrite your code in
    numpy. And, there are a few more tricks you can sometimes use...

3.  Convenience. Numpy has multidiminsional array support, while pure
    Python only supports one diminsional arrays[^1]. There are also lots
    of mathematical functions, too, like `transpose`, `dot`, `cross`,
    etc. that make life much easier with working with certain types of
    data and calculations.

Let’s set a convention before we dive in. There are several ways to load
Numpy. In the shell, many people like to use from `numpy import *`. This
star should should be converted to a list of functions that are used if
used in a script. I don’t like this convention very much for my own use,
and I really don’t like to use it when teaching. So, I’ll use the common
convention `import numpy as np`. This shortens what you have to type,
but also makes it very clear where things are coming from. For example,
if I type `int32`, you might think it was a Python keyword. But if I
type `np.int32`, you know exactly where it came from.

### Array creation

Numpy is built around the powerful `ndarray` class. This can handle any
number of dimensions, and has many, many methods and special methods.
We’ll first look at `array`, which is the most commonly used version,
then we’ll visit `matrix`,

#### Manual

You can use any array-like object to initialize an array. Python lists
and tuples are commonly used. Any object that has an `__array__` special
method will also work. You don’t want to use an iterator by itself, but
you can wrap it in `list()` first. This array-like object can be passed
to the array constructor, and you get a new array with those values
pre-initialized.

Let’s start with creating a new 1D array object:

import numpy as np a=np.array(\[1,2,3\]) b=np.array(\[1.,2,3\])
c=np.array(\[1,2,3\],dtype=np.float32)

Here, all of the arrays contain the three elements `1`, `2`, and `3`.
The first will be an `int32` array, because all the members were `int`.
The second will be a `float64` array, since `float` is more important
than an `int`. The third will be a `float32` array, because it has the
`dtype` keyword argument set.

To create a multidimensional array, you can use nested lists. For
example, `np.array([[1,2],[3,4]])` will create a 2D array.

Other keywords, besides the `dtype` one, are also available. `order`
lets you set the order in memory to be like Fortran instead of C (not
very useful unless mixing with other code)...

#### Common arrays

There are other ways of creating arrays. Most of the time, you probably
will not be entering giant lists in your Python file. In the following
examples, assume we have a shape `shape`, and a preexisting array
`oldarray`. A `shape` is an `int` (1D) or a list of `int`s that describe
the length of each diminsion. Some common arrays are listed. Most of
these have a `dtype` option too.

-   An array of zeros. This can be created as `np.zeros(shape)` or
    `np.zeros_like(oldarray)`. Example: `np.zeros([2,3])`

-   An array of ones. This can be created as `np.ones(shape)` or
    `np.ones_like(oldarray)`.

-   Identity matrix. This is created as `np.eye(N)`, where `N` is the
    size, or `np.eye(N,M)` if you have a rectangular matrix. You can
    also specify a third argument `k` to pick another diagonal.

-   Empty matrix. This is uninitialized; values could be anything. Fast.
    `np.empty(shape)` or `np.empty_like(oldarray)`.

### Array indexing

You can access the elements using normal Python bracket `[]` notation.
Multiple dimensions are handled with commas. `x[0,0]` is the first
element of a 2D array. `x[0]` will return the first row as a 1D array.
You can, therefore, use `x[0][0]` to access the first element also, but
this is internally slower because you are making a temporary array
before returning. You will have to use this second notation if you want
to use array indexing inside a string and then use the `.format` string
method.

### Transforms

[^1]: Actually, built-ins only support one diminsion, but the language
    full support for multiple indexing, like the `Ellipsis` object
    (`...`); these were actually added specifically for Numpy.
