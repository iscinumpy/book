# Installing software

## Choices

You have choices when it comes to installing Python.
The simplest way is to install the Anaconda package.
option. This will install Python 2 or 3 and the conda package manager,
and almost everything you could
ever want for a science stack. Some packages that are normally hard to install
are either included or easier to install using Conda. You'll be able to install
packages from either conda or pip.

You can also install Python manually, or use your own package manger. If you do,
you will have to get packages from pip.

## Conventions

I’ll stick to a few conventions. I’ll use forward slashes for directory
separators (`/`). Notice that this will work in all operating systems; even
if Windows will show backslashes instead by default, it still will
accept a forward slash.

You’ll need to use the command line on Windows or macOS a little. Type `cmd` into the
search to find the command prompt on Windows, or `terminal` to find it on macOS.

## The pip tool

Python has an impressive package management system. This is good, since
you’ll be using a lot of packages! To install a new package that is
listed in the Python Package Index (PyPi), just open a terminal,
(Windows: navigate to or add that directory to the Windows PATH, others
should have it in PATH already), and type `pip install package_name. Python will find the most up
to date package and install it for you.

## Installations

If you choose to install manually, here’s what you need. See the
appendix for a simpler comprehensive listing of standard packages and
locations. You should have the following things installed (in order):

* **Python 3.7 or 2.7**    
    Install from the Python.org website (Windows/Mac) or from a
    repository (Linux). I do not recommend the built-in Mac python, it
    is missing some features. Note that you can install all major
    versions of Python (2.7, 3.5, 3.6, 3.7, etc) in parallel, that is,
    they will work properly installed side by side.

* **Numpy**
* **Scipy**
* **matplotlib**
* **IPython/Jupyter**
    This is the special shell for python. It adds lots of nice shortcuts
    to the language, and some nice looking shells. Install with `conda install jupyter` or `pip install jupyter[all]`.
* **Sympy**

