Getting started in Python
=========================

If you are already familiar with basic Python, skim until you get to the
classes section. If you are comfortable with classes, skim to the Numpy
section. If not, read on!

Python is a powerful, simple, and clean language that is easy to read.
It was meant to be used for anything, not just scientific applications.
If you are completely new to Python, I do recommend that you peruse at
least a little of information on Python for anybody before working
through this book.

There is a ton of great material, like *Dive into Python* and *Dive into
Python3*, by Mark Pilgrim (a personal favorite). Or the harsh, but
probably truer than I would like to admit, *Learn Python the Hard Way*,
by Zed. A. Shaw. I can’t officially agree with many of his comments
about advanced programming, but they are likely true. Plus many more
traditional, and often free, books available. What follows is just a
quick, bare bones intro along with some tips.

> Don’t use iPython if you are just starting! Python comes with IDLE,
> which doesn’t complicate the language with extra features, has helpful
> tips, and is colorful. Get used to what is really part of Python,
> before transitioning to iPython. You'll thank me later.

Variables and types
-------------------

Python is an interpreted, dynamically typed language. To make variables,
just start putting things in them and Python will create them on the
spot. There are two types of variables, mutable (contents can change),
and immutable (contents cannot change). Since, when you reassign a value
to a variable, the old variable gets thrown away, it may be hard to tell
the difference at first. You’ll see some big differences later.

Types are dynamic, that is, you don’t ever have to tell Python that `x`
is a string, you can just write `x=’hi’`. You just assign whatever you
want to `x`, and it gets it’s type automatically. Since the left side of
the equals sign is automatic, the right side needs to be explicit! 3 and
3.0 are different types, one is an integer, the other is a floating
point number. You can ensure a type by writing `x=int(3)` or
`x=float(3)`. This is very important, as it also allows new types - like
Numpy types!

Here are some examples:

x=3 y=3.0 x=’hi’ z=\[1,2,’wow’\]

This creates a variable `x` with an integer 3 stored in it.

This creates a variable `x` with a floating point 3.0 in it.

When x is again assigned to, it got replaced with a brand new `x` that
has a string `’hi’` in it. The old `x` is garbage collected (ie,
trashed).

This is a mutable list (a list that can change). In the 0’s place it has
a `1`, and in the 2’s place it has the string `’wow’`.

[|c|c|c|c|c|]{} Type & Mutable & Description & Example & Alternate
method `bool` & no & Has two values: `True` and `False` & `x=True` &
`x=bool(1)``int` & no & Counting numbers, negative too. & `x=2` &
`x=int(2)``float` & no & Floating point numbers (like decimals). &
`x=2.1` & `x=float(2.1)``str` & no & Strings. Use either single or
double quotes for one line strings, or three in a row for multiple line
strings. A single letter before the string specifies a special type. &
`x=’a string’` & `x=str(’a string’)` `list` & yes & First element is the
0 element. Can hold anything, and change size. &`x=[’a’,7]` &
`x=list([’a’,7])``tuple` & no & Just like a list, but immutable. Uses
normal parentheses, so use a comma for a single item to differentiate
from normal grouping. & `x=(1,2)` or `x=(1,)` & `x=tuple([1,2])``set` &
yes & Only can contain one of each item, and is unordered. & `x={7,1,3}`
& `x=set([7,1,3])``dict` & yes & Dictionary. Each key has an associated
value. Keys can be any immutable types. Values can be any type.
Unordered. Only one of each key. & `x={’a’:1, ’b’:2}` &
`x=dict(a=1,b=2)``complex` & no & Complex numbers. `1j` is not the same
as `j`. & `x=1+2j` & `x=complex(1,2)`

### Indexing

To access a list or tuple element, use bracket `[]` notation. `x[0]` is
the first element of `x`. Negative values will count backward from the
end of the array. `x[-1]` is the final element in the array. You can use
‘slicing’ notation also. `x[1:3]` is the second and third element of the
array; it is the same as writing `[x[1],x[2]]`. You can leave off either
number to indicate beginning or end; `x[:2]` would be elements 0 and 1.
You can also specify a step size, like `1:2:5`. This will produce the
first and third elements. Dictionaries use the same notation only with
keys; use `x[key]` to access the key element of x. The common key type
of string would look like this: `x[’firstkey’]`. Sets do not have an
access notation, since they are unordered and don’t have keys. Any of
these data types can easily be tested for a member object/key by using
key in `x` or element in `x`.

### Last used

If you don’t assign a value, it will appear in the shell. If you type
the underscore by itself, ‘`_`’, it will be the previously output
value[^1].

#### Memory

Let’s talk about how the values in variables are stored. When you write
`x=2`, 2 is placed in memory, and marked as immutable (can’t change
while it is alive). Then it is assigned to `x` (or rather `x` now means
this place in memory with the two in it). If you write `y=2`, something
unusual happens. Python knows it has a 2 already, and it knows that the
2 is constant, so it just makes y point at the same place in memory. So,
if you write `x` is y, you’ll get a True. Because they truly are the
same thing in memory. But nothing bad or surprising can come of this
little quirk, because the 2 will remain. If you now write `x=3`, a new
spot, with a 3 in it is created, and now `x` “points” at that spot. `y`
still is the old, friendly 2.

Now, how about something mutable? `x=[2]`, `y=[2]` may look similar, but
try `x is y`, and you get a `False`. Python knows these can change, so
it uses separate space for both lists. Now try `x=[2], y=x`. `x is y`
will now be `True`. What’s more, if we do something like
`x[0] = 3, y[0]` will now be 3 also.

There are times when you want the same object to have multiple
variables. We’ll use something like that in Numpy. But be warned,
mutability has caught many young Pythoners off guard. You have been
warned.

Functions
---------

Now that we’ve covered variables, let’s talk about something to do with
them. Introducing functions! Functions take 0 or more variables, and
produce 0 or more output variables. You use math-like syntax to call
them. There are several built in functions that can be called without
loading anything new; these are functions like `sin`, `cos`, `abs`,
`pow`, and several more[^2]. If you want to divide `a` by `b` and get
the quotient in `x` and the remainder in `y`, that would be
`x,y = divmod(a,b)`. As you type it, IDLE or iPython (qtconsole or
notebook) will show you a little tip on the function.

There are two ways to denote arguments. The first is called positional
arguments. These are what you just saw with `divmod`; `x` was in the
first position, `y` was in the second. There is a second, very useful
way, called named arguments. If a function has the form
`myfunction(alpha,omega)`, you can write `myfunction(alpha=2,omega=3)`
or `myfunction(2,3)`. It is possible to set up arguments that can only
be reached by using names. It is also possible to set up a default value
for either kind of argument, so that you don’t have to include it when
calling the function.

Outputs from functions are shown on the command line if not assigned (in
fact, anything that is “left over” is shown). The output from a function
with multiple outputs is put into a tuple. If you have the right number
of variables, separated by commas, on the left side, the tuple is
automatically “unpacked” into the variables. This is how the multiple
outputs feature works, and it also allows the use of such nice syntax as
`x,y=y,x`.

You can make your own functions two ways. You can use the normal ’def’
define notation:

def square(x): return x\*x

This defines a function `square`, and it returns the square of `x`. You
can give it anything that supports multiply, like a `float` or an `int`.
After you define a function, you can use it anywhere.

You can also define defaults for your parameters. If we write
`def function(x=1)`, then we can write `function()` or `function(3)` or
`function(x=3)`, and they all work.

You can also use extended parameter syntax. This comes in two parts. In
a function definition, you can have one parameter prefixed by a star.
This, almost unanimously called `*args`, will take 0 or more positional
arguments, whatever is left over, and makes them available as a list in
your function body. After this, you cannot have any positional
arguments. In Python 3, you can have more keyword arguments. Then, you
can have one more parameter, with two stars. This is called almost
unanimously `**kwargs`. This takes all left over keyword arguments and
is a `dict` in your function body.

There is another matching extension to calling functions. If you put a
star before a parameter in the call, then it is assumed to be a list or
tuple and is unpacked in the call. Same thing goes with two stars. So,
the following function calls function 2 exactly the same way function1
was called, no matter how many or what arguments were passed:

def function1(\*args,\*\*kwargs): return function2(\*args,\*\*kwargs)

Besides this use, you can also use this to handle multiple calls, store
function call parameters in dicts or lists, etc.

In Python, blocks start with a colon, and everything inside it is
indented. Indention cannot change in a block. The recommended form of
indent is 4 spaces, but you can use whatever you like, as long as you
are consistent. Most Python editors will cause the tab key to put in
four spaces.

You can also use the odd and rare ’lambda’ notation:

square=lambda x: x\*x

Note that lambda is not a function; it’s just a weird python statement
(more on that later). Don’t worry, you don’t have to use lambdas, you
can always use normal function defs. I just like them because they sound
cool.

### Operators

Now we’ll step back and look at some of the syntax and features of
Python. You’ve already seen operators; most common math operations are
included. I like tables, so here’s one for operators. Notice how I also
include a special name, surrounded by double underscores? This is the
internal python name for the operators. You’ll find this handy when you
want to make your own types (classes section). Any type in Python can
have it’s own behavior defined for operators. So, for example, `*` is
multiply with normal numbers. It works correctly for imaginary numbers.
But if you multiply a string by a number, you’ll get that many copies of
the string. This is very useful in Numpy, since array and matrix
operations can often be more easily and cleanly expressed as operators,
instead of having to use custom functions. But, more on that later.

[|c|c|c|]{} Symbol & Python Name & Description `+` & `__add__` & Adds
objects. Concatenates strings.`-` & `__sub__` & Subtracts objects. `*` &
`__mul__` & Multiplies objects, arrays, strings. `/` & `__div__` &
Divides.`%` & `__mod__` & Modulus. `**` & `__pow__` & Raises to a power.
Do not use a caret!!!

I won’t delve too deeply into operators, but will mention a few common
misunderstandings.

Python’s divide operator changed in Python 3. The old behavior was very
the very common in programming one; return a `float` answer only if one
of the operands was a `float`. So if you wrote `1/2`, you get `0`. If
you wrote `1/2.`, however, you get `0.5`. This was finally fixed in
Python 3, with the addition of the `//` operator. `//` is truncating
divide, and now `/` is float divide. If you like this behavior, you can
use it in Python 2 by including the line
`from __future__ import division` to the top of your code (before
anything else). `//` will work without it (please always use it to show
you meant truncating division when you are doing it intentionally), but
the new behavior of `/` will need this line.

Raising to a power has a couple of forms. To raise `x` to the `y` power,
you can write `x**y` or `pow(x,y)`. You can not write `x^y`. as `^` is
used for something else.

The operators `&`, `|`, `^`, `<<`, `>>`, and `~` are all binary or
“bitwise” operators. You probably won’t need them much. A quick way to
write $2^x$ power is `1<<x`.

Many Python operators are words. `is`, `not`, `and`, `or`, etc. There is
a difference between `is` and `==`, but it’s a subtle one. `is` will
check for the same object, while `==` will check for the same contents.
Due to the way Python works, this only can be seen on mutable objects.
So given `a=2`, `b=2`, `a == b` and `a is b` will both return `True`,
but if you start with `a=[2]`, `b=[2]`, only `==` will be `True`, `is`
will be `False`. Think about which behavior you want. Both are useful.

Logic operators (`and`, `or`) are very special in Python. All Python
objects are either `True` or `False`. `and` returns the last value if
both are `True` or the first `False` value. `or` returns the first
`True` value or the last False value. This allows cool tricks. So that’s
why you occasionally see `and`’s and `or`’s in odd places in some code.
I use this occasionally to cleanly insert a default value if something
is `False`.

Comparisons can be strung together. `1<x<2` is quite valid!

You can use many operators “in place” if you want too by adding an
equals sign. `x+=2` is the same as `x=x+2` (like the C language). But if
the object is mutable, this might be faster, because it doesn’t make a
new object. C’s famous ++ syntax is not supported, though.

Statements
----------

Statements are also built into Python. They are how you control Python.

The `if` statement is a simple and common statement.

if x: y=2 else: y=3

In this simple example, if `x` is `True`, then `y` will be `2`. If it is
not `True`, then `y` will be `3`. You don’t have to have the `else`
block, and you can use `elif` (else-if) blocks after an `if` block to
check more things.

Loops are also very important in programming. To loop, you can use the
`while` statement, and the block will repeat until the condition is
`False`. Please make sure the condition will eventually be `False`
(though Python will try to catch infinite loops and kill them for
you—this has saved me on several occasions). Another important looping
structure is the `for` loop. If you’ve learned this in a language like
C, please unlearn it. Python is a little different. It looks simple.

y=0 for x in range(10): y+=x

This will add up $0 + 1 + 2 + \cdots + 9$ into `y`. `range(10)` is a
built in function that produces a list of numbers, but you could use
anything here. Another list, a tuple, a set, anything that is iteratable
(I’ll define that in more detail in the classes section). If you want to
produce one list from another, there is a very nice shortcut notation:

y=\[x\*\*2 for x in range(5)\]

This will produce in `y` a list of five squared numbers,
`[0, 1, 4, 9, 16]`. If you use `()` brackets, you produce an iterator
instead of a list (more on that later). You can even add an `if` clause
on the end (after `range(5)` in this case)! You can also use `{}` to
produce sets and dictionaries (dictionaries use colons).

You’ve already seen the def statement, and you’ll soon see the similar
class statement. There are a few special statements, like return (leave
a function, optional return value), yield (leave a function but leave it
ready to resume - produces an iterator basically), break (exit a block),
continue (exit a while loop and return to the beginning), and pass (does
nothing, useful if you need an empty block).

Modules
-------

Import is very important, as that’s how you load more stuff into Python
(called modules). Numpy is a module, for example. And there are lots of
standard libraries built in with useful things. You’ve seen a ’fake’
import already; `__future__` is really not a Python module, it’s just a
special way to import a few futuristic features. Once you import a
module, say `import math`, you can use anything in it, for example
taking the sin of 1.4, by typing `math.sin(1.4)`. The part before the
`.` is called the namespace. The current namespace contains all the
things you can directly access, and now the math namespace contains the
math functions.

Submodules also are sometimes used; these are modules inside of modules.
You can import them by using a dot (`.`) in the import statement. Weave,
a submodule of Scipy, can we imported as `import scipy.weave`. Notice
that weave is not accessible until you import it; importing a module
does not generally import all the submodules (the module author can
change default behavior).

There are several ways to import things. I’ll cover them briefly.

-   Import a module: `import module`. This is the fastest
    (computationally) way to import. If you don’t want to use the full
    name of the module all the time, you can use the handy alias
    technique `import module as somethingsimple` to refer to the module
    as `somethingsimple` in your code. Good for long module names.

-   Import all of the module into the current namespace:
    `from module import *`. The star here is pronounced “all”, by the
    way. This will dump everything in the module into your current
    names. So, `from math import *` will allow you to type `sin(1.4)`
    and skip the addition of `math` or any other namespace. This is
    considered “sometimes acceptable” if typing code live, and “frowned
    on” in a script. It can be slower, since all the code is prepared
    for use.

-   Import a part of a module:
    `from module import modulepart1, modulepart2`. This is ideal for
    grabbing just the things you need. They are now in the current
    namespace, and you haven’t cluttered things.

The Python language is rather simple; most of the power things you’ll be
doing are in modules (and you’ll probably write some of your own!). I
can’t cover all modules here, but some commonly used ones are:

The system module. Lots of system stuff.

The operating system module. It’s how you can change directories, etc,
in a nice and platform independent way. (or you could use iPython)

A lot of rather simple math things. Pi, sin, cos, etc.

Lots of iterator stuff.

The powerful regular expression language in Python. Hard to learn/use at
first, but amazingly powerful at string manipulation.

Time and date stuff.

Any module has it’s own help; just type `help(module)` after it is
loaded, or `help(’module’)` before it is loaded. The Python website has
a phenomenal module documentation system also (built-in’s only).

Classes and Objects
-------------------

Here’s the most important section, in my opinion. The way Python is
built make it imperative that you understand and like objects/classes.
Numpy and most other modules use classes heavily. And classes come so
naturally in Python that learning them is a breeze, and opens your eyes
to how actually Python works. You see, everything in Python is an
object.

Everything.

So, everything behaves exactly the same, following the same rules.

*Everything*.

Variables are objects. Functions are objects. Modules are objects.
Strings are objects. Don’t believe me? Type `x = pow`. No error? Now try
`x(3,2)`. You got `9`, didn’t you? You see, the only difference between
a variable-object and a function object is that you put parentheses
after the function objected, that is, you “called it”. You didn’t call
`x` when it was an `int` object. Some are callable, some are not. But
they all could be. It’s just a matter of what behavior is defined.

### Classes

So, let’s start from basics. That would be classes. A class is identical
to what I called a “Type” earlier. It’s a class of things. `int` is a
class. `float` is a class. `Person` could be a class. A class is not a
specific “instance” of something. It just defines what something can do
and what it has to store. To actually use a class, you have to make a
specific instance of it. You have to “call” it to create an objects.
Objects are concrete, and they have values stored in them (probably).

Example? `int` is a class. `x = int(3)` makes an object of the `int`
class, called `x`, that has a value of `3` stored in it. Since `int`’s
are immutable, it would be silly to say `x=int()` and then set the value
later. (Note: that actually assumes a value of `0` if you try it, like I
just did).

You can put things inside an object or class. A variable in an object is
called a “member”. A function in a class is called a “method”. Yes, I
know, both are objects, but historical and functional purposes we still
divide them into two categories and therefore two names. You should
always add functions to a class, though you might occasionally add
variables to the object if you really have to. Most of the time you just
set variables you’ve defined in a class. You access things in a class
using dot notation. Since strings are a class, let’s make a new string
object as an example:

s = ’this is a STRING’

Now, let’s see what’s in string using the handy `dir` function or the
tab key in IDLE/iPython. Quite a lot! Mostly you’ll see functions (if
you used `dir(s)`, ignore anything surrounded by double underscores).
Let’s test one of these:

s.lower() \#\# Output: ’this is a string’

The result? A lowercase string! It was built into `s`, so it already
knew about `s`, so you didn’t have to give it any parameters. It just
worked.

### Special Methods

This already should be giving you ideas. Anything can be represented!
Anything! Vectors! Matrices! Coffee cups! (Oops, mathematician slipped
in) But, there is one more thing. How do Python types (or classes, as we
can now call them) know how to do all those nice things with operators?
How can two strings know how to add themselves? How can functions know
what to do when called? How can lists print nicely on the screen? All
these things are now under your control. Introducing special methods.

Special methods are ones that have a special name. When you try to do
something to an object, Python first tries the appropriate special
method. Like if you write `a+b`, Python tries `a.__add__(b)`. (In this
case, if this doesn’t work, Python will even try `b.__add__(a)`, because
`add` is defined in Python to be communicative.) If your object `a` has
an `__add__` special method that works with that `b`, you can add the
objects. Almost everything in Python has a special method. `a()`
actually does `a.__call__()`. `a` typed by itself in a shell actually
does something like `print(a.__repr__())`.

Please look up a table of special methods. You’ll see many great ones.
I’ll just cover one right now; the `__init__` method.

### Class creation

When you make an an object from a class, two things happen. A
very-hard-to-use-correctly method called `__new__` is called, then the
object is created but is empty, and then `__init__` is called. Except in
a few special cases (I’ll have to bring up at least one in the course of
this book), always use `__init__` to initialize the object. Here’s an
example of an complete class:

import math

class Vector(object): def \_\_init\_\_(self,x,y,z): ”’Creates a new
vector with x, y, and z”’

self.x=x self.y=y self.z=z

print ’\_\_init\_\_ ran successfully’

def norm(self): ”’Returns the norm”’ return
math.sqrt(self.x\*\*2+self.y\*\*2+self.z\*\*2)

This should be fairly clear. First we import the math module, because
we’ll need math.sqrt. Then we make a new class. We always put ’object’
in after the class name, to tell Python 2 that we want to use new style
classes. Python 3 removes old classes so only in Python 3 is it an
acceptable idea to leave this off. Then, we have our `__init__` function
definition, and the first parameter is the self object. A call like
`a.__init__(y)` actually looks like `__init__(a,y)` when you are in the
function. Please only use the word self here. Python will not harm you
for naming it something else, but a million angry Python programmers
will hunt you down and do unspeakable things to you if you name it
anything but self. So, let’s see how this works:

a=Vector(1,2,3) a.x=2 a.norm()

This will produce the norm of `[2,2,3]`. Slick? This is how Numpy was
built. The array class is the backbone of Numpy.

### Hidden Methods and Members

If you are coming from another language, you are probably wondering how
to make things inaccessible or hidden from outside the class. If you are
not coming from another language, this might sound ridiculous. But,
there are times when you do not want the person or program using your
class to “mess up” the way it works. Maybe you want to have a secret
member variable that stores something only one method needs to see. And
only the constructor can set it. How do you do that in Python?

For private variables, the short answer is you can’t. You see, anything
can be done in Python, if you try hard enough. So the only supported
feature is actually good enougth: hiding. There are two ways to hide
variables so they can’t easily be changed outside the class. The first,
and usually good enough way, is to name the variable with a leading
underscore. Like `_x` or `_var`. This will cause most shells/IDE’s to
not show this in the list when you tab, unless you press `_` first. And
this tells all Python programers that this is not something that they
should mess with.

The second way is to prefix two underscores `__` to the name when you
name it. This is even more private than the other method; it will be
there inside the class as `__name`, but from the outside, you’ll have to
use `classname._classname__name` to get to it. Yes, you still can, but
you really have to want to. By the way, this is called “name-mangling”.

I might note that you should never use your own
double-underscore-on-both-sides name. These are supposed to be for
Python; if one is not used, like `__henry__`, there is a chance that the
next release will use it, and that might break your class with
`__henry__` in it. One exception is `__array__`, which Numpy created,
but they have enough clout with Python to be safe. You probably do not.
Also note that these names are not mangled.

### Inheritance

One other quick topic to cover is that of inheritance. Have a class that
has lots of good things, but you want to replace one thing and add one
more thing to it? Easy. Just inherit the old class and redefine or add
anything you like. Here’s how to add a unit function to our vector
class:

class Vector2(Vector): def unit(self):
newvec = self.\_\_class\_\_(self.x, self.y, self.z)

newvec.x /= self.norm() newvec.y /= self.norm() newvec.z /= self.norm()

return newvec

This produces a `newvec` (of type `Vector2`) and normalizes it, then
outputs it. All the old stuff in `Vector` is still there in `Vector2`.
You can always over write anything, including functions, by just making
a new one.

I could have used a class name instead of `self.__class__`, but that
raises a question. Which one? `Vector`, or `Vector2`? Or, for future
inherits, `Vector3`... This is a way to refer to the class of self
without using a specific name. Be the way, `type(self)` is the same[^3]
as `self.__class__`.

Python makes inheritance simple, though there are occasionally gotchas
on complex or specially coded classes. You don’t want to inherit
`numpy.array`, for example. I’ll show you how to correctly inherit that
later if you are interested.

### Static and class methods

Another feature are static methods. These can be called on the class
instead of the object, and usually are used to make new objects. This,
in essence, allows you to have multiple constructors. Just use the
decorator syntax (a line starting with an `@` sign) right before you
define a static method; this would be `@staticmethod` in this case. You
do not pass in `self` in your method `def`, since `self` does not need
to have been created. Static methods are not very useful, except
occasionally for orginization.

If you need the class passed in, use `@classmethod`. Most of the time
you’ll return a new object. This is a very good way to make new classes
using different parameters compared just the default `__init__` method.

Decorators, by the way, have the ability to modify the function after
them. You can look up decorator syntax if you want to, it’s a neat trick
but not very often super-useful.

### Properties (members)

Yet another feature, only in new-style classes, is the ability to have
code run when you access, assign to, or delete a member (variable)[^4].
This can be done with the `property()` function, or the `@property`
decorator. To use the function, just define your functions (names do not
matter), then pass then in to the `property` function to make a new
style member object, that you can simply assign to your class. This
notation is very clumsy, since you have to make a lot of functions just
to work with one property. So the decorator syntax is often used to make
things cleaner. You put `@property` above your function that returns a
property. It’s doc string is the property doc string, it’s name is the
property, and it just takes the normal (`self`) for an argument. It’s
return value is what you get when you access the property. If you need a
way to set the property, use `@mypropertyname.setter` as a decorator
before your set function, and you can reuse the property name (here
`mypropertyname`) as your `setter` function too! (Same goes for the
rarely used `deleter` function)

Think in objects from now on. You have a detector producing a dataset?
That’s an object. The definitions of what data is contained in one run,
and the functions that produce results, those are class members and
methods. A run is an object.

### Decorators

You’ve briefly seen decorators. Since many books do not seem to cover
decorators, I’m including a small section on them. Python’s version of
decorators is very simple, but because it sometimes is hard to think of
functions as objects, can take a little getting used to.

The syntax for using a decorator is simple: Just put a `@` in front of
an object name and it becomes a decorator. The syntax for making a
decorator is... Non-existent! There is no syntax difference between a
normal function and a decorator. Decorators actually are just a little
trick; they call the decorator function with the decorated function,
then put the return value in place of the original function name. This
is important; decorator functions must return functions. If making a
function inside a function sounds weird to you, get over it; in Python,
a function is just an object, and defining a function is just a way of
setting function = definition.

That’s it, that’s all there is. But the idea of decorating functions
covers a lot. Now you see that `@property` and `property()` were just
the same thing written different ways. You can modify the arguments of a
function (like `@staticmethod`), and you can do other things too. There
is a gotcha, though.

The call signature of a function is changed when you make a new one in
it’s place. A signature sign is the thing that you don’t have much
control over, it’s got info that Python uses to show you what the
expected arguments are (when you are in a nice shell or IDE). And the
name of the function (the original name). It’s hard to get at the call
signature; someone wrote a `@decorator` decorator that decorates a
decorator so that it correctly passes this information along (isn’t that
a confusing sentence?).

You can also make decorator factories. These crazy things are simply
just adding an argument on to the decorator. Like
`@this_is_a_factory()`. These decorators need to produce a decorator
(since they are being called first, then being used to decorate). So, if
you were keeping track, they return a function that returns a function!
Why do this? You can add arguments to decorators this way.

You can also decorate classes with class decorators. Exactly the same as
before, only they are right above a `class` block instead of a `def`
block. Oh, yeah, they also return classes. I can’t say I’ve ever seen
one of those used. I think they would make my head hurt too much...

### Getting and setting attributes

Let’s say you somehow ended up with a string that has the name of a
method or member you want to get or set. There is a way to get or set
any property with a string. There are a pair of built-in functions,
`getatter` and `setatter`, that allow you to access by a string. For
example, `getatter(x,’y’)` is the same as `x.y`, and `setatter(x,’y’,3)`
is the same as `x.y=3`. Along the same lines, if you want to access an
object in the current namespace, you can get all the current objects as
a dictionary using `locals()`. There also is a `globals()`; you should
not set the values in `locals()`, only observe, but that does not also
apply to globals (I believe).

Exceptions
----------

Most “errors” in Python, unless they are a bug in compiled code from
another language, are actually under your control. This is done with
`try` and `except` blocks. I’m keeping this part light, so look into it
if you want to go further. Exceptions are great, and will help you
understand how to work out bugs in code.

Simply, any time you ask Python to do something it can’t do, or there is
a `raise` statement in your code. an exception is raised. An object
containing an error or warning is called an exception. The code then
quits, and the exception “bubbles up” through any more calls until it
either hits a `try` block, or the shell. If it makes it to the shell,
you get a glaring error or warning message and a traceback to show you
where it came from and where it “bubbled” up through. If it catches in
the `try` block, it triggers the `except` block. If the `except` block
recognizes the exception, it does some code. It could raise the
exception again. Or it could handle it and just go on. If that
particular exception was not listed in the exceptions to catch, it gets
re-raised automatically. There is also a `finally` block that can do
code no matter what, even if you had an exception you didn’t expect.
This is good for closing files, etc. Don’t be afraid to use exceptions!
Many times, exceptions are just for warnings or even used for good
behavior, like stopping an iteration (`StopIteration`).

To make your own exception, you will usually do something like

class MyNewException(Exception): pass

This makes a new exception called MyNewException. You can now catch it
using `except MyNewException`. You can override the `__init__` function
to add other features, if you need to. But often, this is enough.

Iterators
---------

One last piece of normal Python. Iterators. Love them, hate them, but
they are clean, efficient, and make a lot of Python magic work. An
iterator is something that you can initialize, run 0 or more times, then
it will stop. I’ll first go over the class method of making an iterator,
then show you the shortcut. Most other things, like classes, I’ve done
the other way around, by showing you the short method first. Now you are
grown up and know about classes, I can do it this way.

First, for a class to be an iterator, you need a `__iter__` method. This
just tells the object to “produce something iterable”. It could be the
same object. Or you could return a new class. It doesn’t matter. Usually
it’s the same class, only with a counter initialized. You must return
something iterable.

Then, you need a `.next` method (In Python 3, this is corrected to be a
`__next__` method). This produces whatever you need to use from your
class. When you are done, you have to raise a `StopIteration`. This is
just how it signals a stop, and is not an error. Not having one it an
error, though! Now you can use it in any iterable situation, like in the
`list()` constructor or in a `for` loop. An iterators may only work
once. Here’s an example:

class Itt(object): def \_\_iter\_\_(self,length): self.i=0 self.end =
length return self def next(self): self.i+=1 if self.i&gt;self.end:
raise StopIteration else: return self.i

To use, try `a=Itt(4)`, then `list(a)`. You’ll get a list that looks
like `[1, 2, 3, 4]`.

This is great for many other things, like reading files. A less powerful
but quick shortcut is to use the `yield` keyword in a function
definition. On the first call to next, the function will go until it
hits the yield keyword, then it will wait until next is called again,
then continue. When it ends (or hits a `return`), it will
`StopIteration`.

An iterator function is not an iterator; it produces iterators. So, for
an example

def itt(a): x=0 while x&lt;a: x+=1 yield(x)

Then, `a=Itt(4); list(a)`or the simpler `list(itt(4))` will produce
`[1,2,3,4]`.

The final way to make iterators is with a generator shortcut. This is
just like the list shortcut covered earlier.

With
----

Another handy block is the `with` block. It allows you to nicely ‘start’
and ‘stop’ using something. Anything with `__enter__` and `__exit__`
methods can be used in a `with`. These are great for files, which need
to be closed after using. Here’s how you open a file nicely in Python:

with open(’filename’) as file: x=file.read()

As soon as the block ends, the file is closed—even if there was an
exception while reading the file. This uses two special methods,
`__enter__` and `__exit__`, that are called on entering and exiting the
`with` block.

Magics (iPython)
----------------

If you are new to iPython, one of the main things it does is extent the
Python language with something it calls “magic” functions. These are
intercepted by iPython before they get to Python, and turned into valid
Python code. Why is this useful? It allows much easier file manipulation
from inside of Python for one. cd, dir, pwd, etc. all work. It makes the
Python shell work more like a normal shell.

Magic functions can often be typed as they appear, like `cd`. However,
if you have an object named `cd`, then you must use the full iPython
name: `%cd`. All magic functions have a `%` sign at the front. If you
are writing an iPython script (a Python script with iPython commands in
it), then you need the `%`’s. `%lsmagic` will list all magics. Magics do
not follow Python syntax, so `cd .` will change the directory to your
home directory, no need for quotes, etc.

iPython also has a build in help system that can be activated by
prepending or affixing the `?` character to any command. It is very
smart and leverages the built in help tools too. `??` before or after a
command will give you way too much help. You can also use tab to get it
to try very, very hard to show you everything you can finish typing a
command with. It will look into directories, modules, etc. to try to
find suggestions.

iPython also can be started in qtconsole mode, or in notebook mode. The
former allows some nice features, like tooltips, menus, and multiline
editing. And notebook mode works in a browser much like Sage or
Mathematica.

iPython can also be started with several options. imports a lot of
things like numpy. Adding will allow plots to appear in the qtconsole or
in the web broswer.

Help
----

Python has an amazing built in help system. You can use `help()` on any
module, class, or function to see the built in doc strings.

If you have a question on a function in Python or Numpy (etc), always
try Python’s own help first. There are usually details on all possible
parameters, examples, and everything else you need. Use this as a
template for your own functions.

Working with your code
----------------------

### Debugging

Your code will not work the first time. It’s a rule for all computers
and languages. So, what do you do when it doesn’t work? Normally, you
can guess from the traceback. But if you can’t tell why it failed or
there was no actual error (just bad results), there still is hope.
Python has a built in debugger module, `pdb`. And iPython even has
magics that let you run it after the crash occurred (`%debug` as soon as
it throws the error).

### Profiling

Python has several built in profilers for you code. Before trying to
speed up a slow piece of code, run it through a profiler. That will tell
you what is slow, so that you don’t have to spend time reducing the
runtime of things that are taking 0.01% of your runtime anyway.

Python profilers tend to work best on function calls, not on
line-by-line analysis, so a good tip when writing is keep things fairly
well separated into functions.

### Documentation

Python has a built in documentation system. You can actually document
your code as you write it. How? A normal comment, one proceeded by a `#`
sign, will be ignored by Python. But, a lone string or multiline string
will be turned into help if it is in the right place. (A lone string is
one that is by itself on a line, not assigned to anything). Right after
a `def` statement (or `class`) or an assignment, you can put a string
with help in it. This will show up as a tool tip in some editors, and
will be accessible with `help()` called on the object it documents, and
iPython’s `?` magics uses it too. It counts as a statement, too, so many
people advocate using only doc strings instead of `pass` statements when
making an outline of empty functions in a class.

Python is even smart enough to know that when you request help on a
object that contains things (like a class or a module), it needs to give
you help on all the parts, too. Know it, use it, love it. Anyone who
uses your code will thank you for it, and in a year, you’ll thank me for
it too.

To make an HTML document for your entire module of all the objects in
it, you can simply run the script on the module and use the option to
get a useful webpage saved for your module. The webpage even has nice
links in it to make navigation easier.

Also, when naming your functions, think about how it should be called,
not what it does. `I_return_the_length` is not a good name;
`calculate_length` or just `length` is better. (And the `__len__`
special method might be best, in this case).

#### Sphinx

You may notice that almost all Python documentation has a uniform, very
clean look to it, with lots of features like code example formatting.
This is because they used the tool Sphinx to build their documentation.
Sphinx is a tool that you might call ‘LaTeX for documentation’. It’s not
automatic; in fact, you can avoid using the doc strings at all if you
really want to. And it takes a little time to learn to write and run
your document generation code. But the result, both in beauty,
flexibility, and power, is unmatched (for Python; Doxygen for C++ is
also powerful too). You can use mathematical display for equations and
symbols, graphics, and you can even include inline generated plots. You
can compile to html, pdf (through LaTeX), man page, epub, or even mobile
html through an extension module sphinx-bootstrap-theme. You can also
use it for other languages, or even to write a book.

### Packaging

Packaging really nice, but also is a bit of a mess. Python has excellent
package creation tools. The problem is the “s” after “tool”. There are
several ways to package, several ways to build. Which is best? It
depends on which camp you are in, and if you use Python 2 or 3. I
recommend, at the moment, to use the built in `distutils` for building
and packaging, and the other tools, like distribute and setuptools, for
downloading modules from (and to?) the repository.

Compiling C code for use as a module is actually very easy if you use
`distutils`. I’ll go into compiled extensions later as a way to extend
Numpy, but `distutils` and `ctypes` are both useful pure-python tools
too. Feel free to look at those sections even if you don’t need numpy.

[^1]: iPython extends this to allow you to recall as far back as you
    want to go

[^2]: Built-in function list at
    http://docs.python.org/library/functions.html

[^3]: Almost all the time, there is a minor exception.

[^4]: In C, this is similar to using get\_ and set\_ functions. Please
    do not write Python with get and set functions. Use properties
    instead. That’s why properties exist in Python! (and why all
    attributes are public)
