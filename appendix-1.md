Appendix
========

     Module    Description                                                       Py(x,y)      Win Binary     Cygwin          Linux
  ------------ ---------------------------------------------------------------- ---------- ---------------- --------- -------------------
   Python 2.7  Also called C python.                                             Included     python.org     Package        python
   Python 3.3  Use instead of 2.7. Some packages not available!                     NA        python.org     Package        python3
     Numpy     Main numerics package. Arrays, type control, numeric routines.    Included         -          Package     python-numpy
     Scipy     Scientific routines. Mat file IO.                                 Included         -          Package     python-scipy
   Matplotlib  Plotting tools, mostly 2D.                                        Included   matplotlib.org   Package   python-matplotlib


